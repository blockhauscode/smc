'use strict'
const moment = require('moment')
const app = require('../../server/server')

module.exports = function (Settings) {
    Settings.setLastImport = async function () {
        const settings = await Settings.findById('5ef097260112c800333d9fbc')

        // sub 5 minutes bacause I want to call it after import execution that takes some time
        const now = moment().subtract(2, 'minutes').toISOString()
        settings.updateAttributes({
            lastImport: now,
        })
    }

    /**
     * Hook triggered after each instance save operation
     */
    Settings.observe('after save', (ctx, next) => {
        const { Mo } = app.models
        /* Set default role for new instances of account */
        if (!ctx.isNewInstance) {
            // clean mo older than lastImport
            const where = { updatedAt: { lte: ctx.instance.lastImport } }
            Mo.destroyAll(where)
        }
        next()
    })
}
