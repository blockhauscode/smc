import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../input'
import Select from 'react-select'

const validate = (values) => {
    const errors = {}
    if (!values.name) {
        errors.name = 'Required'
    } else if (values.name.length > 20) {
        errors.name = 'Must be 20 characters or less'
    }
    return errors
}

export class AdminForm extends Component {
    constructor(props) {
        super(props)
    }

    onAdminFormButtonPress = () => {
        this.props.onSubmitForm()
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    placeholder="Mario"
                    type="text"
                    name="firstName"
                    label="Nome"
                    component={Input}
                />
                <Field
                    placeholder="Rossi"
                    type="text"
                    name="lastName"
                    label="Cognome"
                    component={Input}
                />
                <Field
                    placeholder="mario.rossi@smc.com"
                    type="text"
                    name="email"
                    label="Email"
                    component={Input}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onAdminFormButtonPress}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'AdminForm',
    validate,
})(AdminForm)
