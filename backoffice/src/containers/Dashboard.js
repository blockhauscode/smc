import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router-dom'
import { withContainer, connect } from '../services/container'
import PageTitle from '../components/pageTitle'
import LoggedNavbar from '../components/loggedNavbar'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faThLarge,
    faCogs,
    faBoxes,
    faFileAlt,
    faUserAstronaut,
    faFileImport,
    faEnvelope,
} from '@fortawesome/free-solid-svg-icons'
const dashboardLinks = [
    { icon: faBoxes, name: 'Gestione Aree', to: '/manageAreas' },
    { icon: faThLarge, name: 'Gestione Macchine', to: '/manageMachines' },
    { icon: faUserAstronaut, name: 'Gestione Operatori', to: '/manageWorkers' },
    { icon: faFileAlt, name: 'Planner Manuale', to: '/planner' },
    { icon: faFileImport, name: 'Import', to: '/import' },
    { icon: faCogs, name: 'Settings', to: '/settings' },
    { icon: faEnvelope, name: 'Centro Messaggi', to: '/messages' },
]

export class Dashboard extends Component {
    componentDidMount() {}

    render() {
        return (
            <div>
                <LoggedNavbar />
                <PageTitle title=" SMC Dashboard" />
                <div className="flex items-center flex-wrap justify-center">
                    {dashboardLinks?.map((link) => (
                        <div className="border border-blue w-56 mx-20 my-10 p-4 bg-blue-200 rounded">
                            <Link
                                to={link.to}
                                className="flex flex-col align-center items-center "
                                key={`link_${link.name}`}>
                                <FontAwesomeIcon
                                    icon={link.icon}
                                    color="#fff"
                                    size={'lg'}
                                />
                                <span className="text-blue-500 pt-4 hover:text-white">
                                    {link.name}
                                </span>
                            </Link>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({}) => ({})

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(Dashboard))
