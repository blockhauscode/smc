import { ACTION_editInfo } from './types'

const editInfo = ({ rest }, reference) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const { info } = getState().form.InfoCodeForm.values
    const request = rest.patch(
        `infos/${reference}`,
        {
            text: info,
        },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_editInfo,
    })
}

export default editInfo
