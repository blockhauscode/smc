import { ACTION_createDepartment } from './types'

const createDepartment = ({ rest }, payload) => (dispatch, getState) => {
    // const config = { params: { access_token: getState().user.token } }
    const config = {
        params: {
            access_token: getState().user.token,
        },
    }
    const { name } = getState().form.DepartmentForm.values
    const request = rest.post(`departments`, { name }, config)

    return dispatch({
        payload: request,
        type: ACTION_createDepartment,
    })
}

export default createDepartment
