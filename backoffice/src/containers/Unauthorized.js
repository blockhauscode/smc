import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { withContainer, connect } from '../services/container'
import PageTitle from '../components/pageTitle'
import LoggedNavbar from '../components/loggedNavbar'

export class Unauthorized extends Component {
    render() {
        return (
            <div>
                <LoggedNavbar />
                <PageTitle title="Unauthorized" />
                <div className="flex items-center flex-col">
                    Le funzionalitá di controllo sono consentite esclusivamente
                    agli account amministrativi
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({}) => ({})

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(Unauthorized))
