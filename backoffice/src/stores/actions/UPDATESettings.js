import { ACTION_UpdateSettings } from './types'

const UPDATESettings = ({ rest }) => (dispatch, getState) => {
    const settingsId = getState().user.settings.id
    try {
        const payload = {}
        if (getState().form.CLForm) {
            const { cl } = getState().form.CLForm.values
            payload.cl = cl
        } else if (getState().form.PathForm) {
            const { path } = getState().form.PathForm.values
            payload.path = path
        }

        const request = rest.patch(`settings/${settingsId}`, payload)
        return dispatch({
            payload: request,
            type: ACTION_UpdateSettings,
        })
    } catch (error) {
        console.log(error)
    }
}

export default UPDATESettings
