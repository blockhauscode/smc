import { ACTION_resetPassword } from './types'

const postResetPassword = ({ rest }, payload) => (dispatch, getState) => {
    // const config = { params: { access_token: getState().user.token } }
    const config = null
    const request = rest.post(`http://localhost:3000`, payload, config)

    return dispatch({
        payload: request,
        type: ACTION_resetPassword,
    })
}

export default postResetPassword
