import { ACTION_sendMessages } from './types'

const sendMessages = ({ rest }) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const { subject, text, receivers } = getState().form.MessageForm.values
    const senderId = getState().user.id
    const data = receivers?.map((x) => ({
        receiverId: x.value,
        senderId,
        senderType: 'account',
        receiverType: 'worker',
        subject,
        text,
    }))

    const request = rest.post(`messages/multiSend`, { data }, config)

    return dispatch({
        payload: request,
        type: ACTION_sendMessages,
    })
}

export default sendMessages
