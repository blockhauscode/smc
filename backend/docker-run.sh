#!/usr/bin/env sh
export NODE_ENV

# Adds to this shell path the argument passed
: ${OIFS:=$IFS}
path_add() {
        IFS=':'
        for dir in $PATH; do
                IFS="$OIFS"
                if test "$dir" = "$1"; then
                        return
                fi
        done
        IFS="$OIFS"

        export PATH="$1${PATH:+:$PATH}"
}

#Adds /node_modules/.bin to the shell path
path_add "/node_modules/.bin"
#Adds /code/node_modules/.bin to the shell path
path_add "/code/node_modules/.bin"

#We install node_modules outside project directory in order to
#avoid that developers machine node_module create chaos
cp -f "/code/package.json" /
cp -f "/code/package-lock.json" / 2>/dev/null || true

if [ "$ENV" = pro ]; then
        : ${NODE_ENV:=production}
else
        : ${NODE_ENV:=development}
fi

#Create subshell and install dependencies
cd /
# When NODE_ENV=production, npm skips installation of devDependencies.
npm install

# We free up container from not needed stuff.
npm cache clean --force
