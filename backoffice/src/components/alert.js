import React from 'react'
import PageTitle from './pageTitle'
import Button from './button'

const Alert = ({
    title,
    onPositive,
    onNegative,
    positiveText = 'Si',
    negativeText = 'No',
}) => {
    return (
        <div className="overlay">
            <div className="alert">
                <PageTitle title={title} />
                <div className="flex justify-end">
                    {onNegative && (
                        <Button
                            onClick={onNegative}
                            text={negativeText}
                            style="px-4 self-end ml-2"
                        />
                    )}
                    {onPositive && (
                        <Button
                            onClick={onPositive}
                            text={positiveText}
                            style="px-4 self-end ml-2"
                        />
                    )}
                </div>
            </div>
        </div>
    )
}

export default Alert
