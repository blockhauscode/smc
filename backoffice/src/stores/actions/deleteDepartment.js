import { ACTION_deleteDepartment } from './types'

const deleteDepartment = ({ rest }, departmentId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const request = rest.delete(`departments/${departmentId}`, config)

    return dispatch({
        payload: request,
        type: ACTION_deleteDepartment,
    })
}

export default deleteDepartment
