import { ACTION_deleteArea } from './types'

const deleteArea = ({ rest }, areaId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const request = rest.delete(`areas/${areaId}`, config)

    return dispatch({
        payload: request,
        type: ACTION_deleteArea,
    })
}

export default deleteArea
