import { ACTION_signup } from './types'

const postSignup = ({ rest }, payload) => (dispatch, getState) => {
    // const config = { params: { access_token: getState().user.token } }
    const config = null
    const request = rest.post(`http://localhost:3000`, payload, config)

    return dispatch({
        payload: request,
        type: ACTION_signup,
    })
}

export default postSignup
