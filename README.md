# SMC

SMC uses [Docker]([[https://docs.docker.com/install/](https://docs.docker.com/install/)]) and [docker-compose]([[https://docs.docker.com/compose/](https://docs.docker.com/compose/)]) to handle development and production environments.
The whole architecture consists of 3 main services:

- *database*: a [MongoDB]([[https://docs.mongodb.com/manual/](https://docs.mongodb.com/manual/)]) instance
- *backend*: a REST service with   [loopback v3]([https://loopback.io/doc/en/lb3/])
- *backoffice*: a [React]([[https://reactjs.org/](https://reactjs.org/)]) web application

## DEVELOPMENT MODE

`docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d --build`

*Or if you already built it*

`docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d`

You have now your system up and running with hot-reloading as it was running on your local machine without docker.

*NOTES*

Since your local machine and the container have different architectures,  `node_modules`  could cause compatibility problems. If you need to install something, do it normally (e.g `npm install something --save`), but recall  your colleagues to remove the container (`docker container rm -f container-name`) and rebuild it from scratch if they have to use your work. 
Moreover, it's a good practice to remove your *node_modules* local folder from time to time  and rebuild the container from scratch, in order to have a fresh service installation in your container.


## PRODUCTION MODE

`docker-compose -f docker-compose.yml -f docker-compose.pro.yml up -d --build`

## USEFUL COMMANDS

**CONTAINER SHELL BASH**  

`docker exec -it container-name bash `


**CONTAINER DELETION (need to rebuild after)**

`docker container rm -f container-name`

**UP  DEV (-d == detach - non blocking shell)**

`docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d`

**UP & BUILD DEV (-d == detach - non blocking shell)**

`docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d --build`

**UP & BUILD PROD (-d == detach - non blocking shell)**

`docker-compose -f docker-compose.yml -f docker-compose.pro.yml up -d --build`

**UP & BUILD PROD SINGLE SERVICE (-d == detach - non blocking shell)**  

`docker-compose -f docker-compose.yml -f docker-compose.pro.yml up -d --build service-name`

**SHOW LOGS FROM THE CONTAINER**  
`docker logs -f 3f7937cd3acd`

**STOP AND REMOVE ALL CONTAINERS**
`docker-compose -f docker-compose.yml -f docker-compose.pro.yml down --rmi all`