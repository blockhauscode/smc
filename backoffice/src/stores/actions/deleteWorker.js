import { ACTION_deleteWorker } from './types'

const deleteWorker = ({ rest }, workerId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const request = rest.delete(`workers/${workerId}`, config)

    return dispatch({
        payload: request,
        type: ACTION_deleteWorker,
    })
}

export default deleteWorker
