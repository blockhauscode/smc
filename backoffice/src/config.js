export const dev_URL = 'http://localhost:3000/api/'

export const prod_URL = 'http://ITF000050S.smcitalia.it:3000/api/'

// export const baseURL = dev_URL
// export const baseURL = prod_URL

export const baseURL =
    process.env.NODE_ENV === 'production' ? prod_URL : dev_URL
