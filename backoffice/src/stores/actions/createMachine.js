import { ACTION_createMachine } from './types'

const createMachine = ({ rest }, payload) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
        },
    }
    const {
        code,
        description,
        shortDescription,
        f1,
        f2,
        f3,
        area,
        operation,
    } = getState().form.MachineForm.values
    const request = rest.post(
        `machines`,
        {
            code,
            description,
            shortDescription,
            f1,
            f2,
            f3,
            areaId: area.value,
            operation,
        },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_createMachine,
    })
}

export default createMachine
