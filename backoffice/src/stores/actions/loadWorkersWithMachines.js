import { ACTION_loadWorkersWithMachines } from './types'

const loadWorkersWithMachines = ({ rest }) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
            filter: { include: 'machines' },
        },
    }
    const request = rest.get(`workers`, config)

    return dispatch({
        payload: request,
        type: ACTION_loadWorkersWithMachines,
    })
}

export default loadWorkersWithMachines
