module.exports = Model => {
    /**
     * Add a timestamp to track the creation time of the instance
     */
    Model.defineProperty('createdAt', { type: Date, default: '$now' })

    /**
     * Add a timestamp to track the last time of updating the instance
     */
    Model.defineProperty('updatedAt', { type: Date, default: '$now' })

    /**
     * Set updatedAt with current time at each instance update
     */
    Model.observe('before save', (ctx, next) => {
        if (!ctx.isNewInstance) {
            ctx.data.updatedAt = new Date()
        }
        next()
    })
}
