'use strict'

const app = require('../../server/server')

module.exports = function(Department) {
    Department.observe('before delete', async ctx => {
        const { Area } = app.models

        // Check if the department has linked areas
        const areas = await Area.find({ where: { departmentId: ctx.where.id } })

        const departmentHasLinkedAreas = areas.length > 0
        if (departmentHasLinkedAreas) {
            throw Error("Can't delete a department with linked areas")
        }
    })
}
