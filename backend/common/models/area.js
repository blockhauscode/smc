'use strict'

const app = require('../../server/server')

module.exports = function(Area) {
    Area.observe('before delete', async ctx => {
        const { Machine } = app.models

        // Check if the area has linked machines
        const machines = await Machine.find({ where: { areaId: ctx.where.id } })

        const areaHasLinkedMachines = machines.length > 0
        if (areaHasLinkedMachines) {
            throw Error("Can't delete an area with linked machines")
        }
    })
}
