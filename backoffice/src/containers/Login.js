import React, { Component } from 'react'

import { bindActionCreators } from 'redux'
import { Redirect } from 'react-router-dom'
import { withContainer, connect } from '../services/container'
import LoginForm from '../components/forms/Login'
import LoggedNavbar from '../components/loggedNavbar'
import { login } from '../stores/actions'

export class Login extends Component {
    state = {
        redirectToReferrer: false,
        unauthorizedUser: false,
    }

    componentDidMount() {
        if (this.props.user.token) {
            this.setState(() => ({
                redirectToReferrer: true,
                unauthorizedUser: this.props.user.role !== 'admin',
            }))
        }
    }

    onLoginSubmit = async () => {
        try {
            await this.props.login()
            this.setState(() => ({
                redirectToReferrer: true,
                unauthorizedUser: this.props.user.role !== 'admin',
            }))
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        const { redirectToReferrer, unauthorizedUser } = this.state
        const { from } = this.props.location.state || {
            from: {
                pathname: unauthorizedUser ? '/unauthorized' : '/dashboard',
            },
        }

        if (redirectToReferrer) {
            return <Redirect to={from} />
        }

        return (
            <div>
                <LoggedNavbar links={[{ name: '', to: '/' }]} />
                <LoginForm onSubmitForm={this.onLoginSubmit} />
            </div>
        )
    }
}

const mapStateToProps = ({ user }) => ({ user })

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            login,
        },
        dispatch,
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(Login))
