import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../../components/input'

const validate = values => {
    const errors = {}
    if (!values.email) {
        errors.email = 'Required'
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }
    return errors
}

export class Reset extends Component {
    constructor(props) {
        super(props)
    }

    onResetSubmit = () => {
        this.props.onSubmitForm()
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    placeholder="Email"
                    type="email"
                    name="email"
                    component={Input}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onResetSubmit}>
                    Sign in
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'Reset',
    validate,
})(Reset)
