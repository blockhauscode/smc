import { ACTION_editNote } from './types'

const editMo = ({ rest }, moId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const { note } = getState().form.NoteForm.values
    const request = rest.patch(`mos/${moId}`, { note }, config)

    return dispatch({
        payload: request,
        type: ACTION_editNote,
    })
}

export default editMo
