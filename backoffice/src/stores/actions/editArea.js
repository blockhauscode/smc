import { ACTION_editArea } from './types'

const editArea = ({ rest }, areaId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const { name } = getState().form.AreaForm.values
    const request = rest.put(`areas/${areaId}`, { name }, config)

    return dispatch({
        payload: request,
        type: ACTION_editArea,
    })
}

export default editArea
