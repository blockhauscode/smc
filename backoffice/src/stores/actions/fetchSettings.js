import { ACTION_fetchSettings } from './types'
import 'moment/locale/it'

const fetchSettings = ({ rest }) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
        },
    }

    const request = rest.get(`/Settings`, config)

    return dispatch({
        payload: request,
        type: ACTION_fetchSettings,
    })
}

export default fetchSettings
