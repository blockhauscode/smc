import React from 'react'
import { Link } from 'react-router-dom'

class PageTitle extends React.Component {
    render() {
        return (
            <div className="bg-blue-400 h-10 flex items-center justify-around">
                <div className="text-white">{this.props.title}</div>
            </div>
        )
    }
}

export default PageTitle
