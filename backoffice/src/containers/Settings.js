import React, { Component } from 'react'
import moment from 'moment'
import { bindActionCreators } from 'redux'
import { withContainer, connect } from '../services/container'
import DataTable from 'react-data-table-component'
import DatePicker from 'react-datepicker'
import Modal from 'react-modal'
import LoggedNavbar from '../components/loggedNavbar'
import PageTitle from '../components/pageTitle'
import { fetchSettings, UPDATESettings } from '../stores/actions'
import 'react-datepicker/dist/react-datepicker.css'
import Button from '../components/button'
import CLForm from '../components/forms/CLForm'
import PathForm from '../components/forms/PathForm'

import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons'

export class Settings extends Component {
    state = {
        editMode: false,
        editModePath: false,
    }
    componentDidMount() {
        this.props.fetchSettings()
    }

    openEdit() {
        this.setState({
            editMode: true,
        })
    }

    closeEdit() {
        this.setState({
            editMode: false,
        })
    }

    openEditPath() {
        this.setState({
            editModePath: true,
        })
    }

    closeEditPath() {
        this.setState({
            editModePath: false,
        })
    }

    onEditFormSubmit = async () => {
        await this.props.UPDATESettings()
        this.closeEdit()
        this.closeEditPath()
    }

    render() {
        return (
            <div>
                <LoggedNavbar
                    links={[{ name: 'Dashboard', to: '/dashboard' }]}
                />
                <PageTitle title="Settings" />

                <div className="flex items-center flex-col pt-4">
                    <div>
                        Vengono caricate le lavorazioni in conto lavoro con un
                        anticipo di {this.props.user.settings.cl} giorni
                    </div>
                    <button
                        className="btn btn-blue"
                        type="button"
                        onClick={() => this.openEdit()}>
                        Modifica
                    </button>
                </div>

                <div className="flex items-center flex-col pt-4">
                    <div>
                        Il base path per il caricamento pdf é{' '}
                        {this.props.user.settings.path} con sottocartelle
                        archivio_disegni_std e mo-pdf
                    </div>
                    <button
                        className="btn btn-blue"
                        type="button"
                        onClick={() => this.openEditPath()}>
                        Modifica
                    </button>
                </div>

                <Modal
                    isOpen={this.state.editMode}
                    onRequestClose={() => this.closeEdit()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeEdit()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle
                        title={'Modifica anticipo di caricamento conto lavoro'}
                    />
                    <CLForm
                        onSubmitForm={this.onEditFormSubmit}
                        initialValues={10}
                        actionText={'Salva Modifiche'}
                    />
                </Modal>

                <Modal
                    isOpen={this.state.editModePath}
                    onRequestClose={() => this.closeEditPath()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeEditPath()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle title={'Modifica base path risorse pdf'} />
                    <PathForm
                        onSubmitForm={this.onEditFormSubmit}
                        initialValues={'http://'}
                        actionText={'Salva Modifiche'}
                    />
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = ({ main, user }) => ({ main, user })

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            fetchSettings,
            UPDATESettings,
        },
        dispatch,
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(Settings))
