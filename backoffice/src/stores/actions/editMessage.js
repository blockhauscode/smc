import { ACTION_editMessage } from './types'

const editMessage = ({ rest }, messageId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const request = rest.patch(
        `messages/${messageId}`,
        {
            hidden: true,
        },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_editMessage,
    })
}

export default editMessage
