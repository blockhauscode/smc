import { ACTION_editMachine } from './types'

const editMachine = ({ rest }, machineId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const {
        code,
        description,
        shortDescription,
        f1,
        f2,
        f3,
        area,
        operation,
    } = getState().form.MachineForm.values
    const request = rest.put(
        `machines/${machineId}`,
        {
            code,
            description,
            shortDescription,
            f1,
            f2,
            f3,
            areaId: area.value,
            operation,
        },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_editMachine,
    })
}

export default editMachine
