import React from 'react'
import { ActivityIndicator, View } from 'react-native'

export default function LoadingWrapper({ children, ready }) {
    return ready ? <>{children}</> : <div>Loading...</div>
}
