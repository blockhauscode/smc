#!/usr/bin/env sh

# Adds to this shell path the argument passed
: ${OIFS:=$IFS}
path_add() {
        IFS=':'
        for dir in $PATH; do
                IFS="$OIFS"
                if test "$dir" = "$1"; then
                        return
                fi
        done
        IFS="$OIFS"

        export PATH="$1${PATH:+:$PATH}"
}

#Adds /node_modules/.bin to the shell path
path_add "/node_modules/.bin"
#Adds /code/node_modules/.bin to the shell path
path_add "/code/node_modules/.bin"

if [ "$ENV" = pro ]; then
        npm run build
else
        npm run start
fi
