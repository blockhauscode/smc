import { ACTION_createArea } from './types'

const createArea = ({ rest }, departmentId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const { name } = getState().form.AreaForm.values
    const request = rest.post(
        `departments/${departmentId}/areas`,
        { name },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_createArea,
    })
}

export default createArea
