import { ACTION_loadMos } from './types'
import moment from 'moment'

const loadMos = ({ rest }, dateStart, dateEnd) => (dispatch, getState) => {
    const between = [
        moment(dateStart).startOf('day').toISOString(),
        moment(dateEnd).endOf('day').toISOString(),
    ]
    const config = {
        params: {
            access_token: getState().user.token,
            filter: {
                where: {
                    dateT: { between: between },
                },
                order: 'dateT ASC',
            },
        },
    }
    const isWorkerSelected = getState().form.WorkerSelect.values
    if (isWorkerSelected) {
        const workerId = isWorkerSelected.worker.value
        const worker = getState().main.workers.find((x) => x.id === workerId)
        console.log(worker)
        config.params.filter.where.machineCode = {
            inq: worker.machines?.map((x) => x.code),
        }
    }
    const request = rest.get(`mos`, config)

    return dispatch({
        payload: request,
        type: ACTION_loadMos,
    })
}

export default loadMos
