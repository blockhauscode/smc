import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../input'

export class NoteForm extends Component {
    constructor(props) {
        super(props)
    }
    
    onNoteFormButtonPress = () => {
        this.props.onSubmitForm()
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    name="note"
                    component={(props) => (
                        <Input
                            {...props}
                            isArea={true}
                            type="text"
                            name="note"
                            label="Info MO"></Input>
                    )}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onNoteFormButtonPress}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'NoteForm',
})(NoteForm)
