import { ACTION_editWorker } from './types'

const editWorker = ({ rest }, workerId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const {
        firstName,
        lastName,
        email,
        machines,
        role,
        folder,
        tagli
    } = getState().form.WorkerForm.values
    const request = rest.put(
        `workers/${workerId}`,
        {
            firstName,
            lastName,
            email,
            role,
            password: 'smc',
            folder,
            tagli,
            machines: machines?.map(x => x.value),
        },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_editWorker,
    })
}

export default editWorker
