import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import UserReducer from './reducer_user'
import MainReducer from './reducer_main'
import MessagesReducer from './reducer_Messages'

export default combineReducers({
    user: UserReducer,
    main: MainReducer,
    messages: MessagesReducer,
    form: formReducer,
})
