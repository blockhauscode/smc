import { ACTION_deleteMachine } from './types'

const deleteArea = ({ rest }, machineId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const request = rest.delete(`machines/${machineId}`, config)

    return dispatch({
        payload: request,
        type: ACTION_deleteMachine,
    })
}

export default deleteArea
