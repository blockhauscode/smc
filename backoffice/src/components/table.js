import React from 'react'
import Button from './button'

const Table = ({ columns, items }) => {
    const renderContent = (col, item) => {
        if (col.contentType === 'itemProp') {
            const keyArray = col.key.split('.')
            if (keyArray.length === 1) {
                return item[col.key]
            } else if (keyArray.length === 2) {
                // maybe use lodash
                return item[keyArray[0]] ? item[keyArray[0]][keyArray[1]] : 0
            }
            console.error(col.key, 'max two level')
            return ''
        } else if (col.contentType === 'fixed') {
            return col.fixed
        } else if (col.contentType === 'method') {
            return col.method(item)
        } else if (col.contentType === 'actions') {
            return (
                <div className="flex flex-row items-center justify-center ">
                    {col.actions?.map((action, i) => (
                        <Button
                            onClick={() => action.onClick(item)}
                            key={`dep_${i}`}
                            text={action.name}
                            icon={action.icon}
                            style="px-4 py-2 mr-2"
                        />
                    ))}
                </div>
            )
        }
    }

    return (
        <table className="border-collapse w-full">
            <thead>
                <tr>
                    {columns?.map(col => (
                        <th
                            className="p-3 bg-blue-200 text-blue-600 border border-blue-300 hidden lg:table-cell"
                            key={`col_th_${col.title}`}>
                            {col.title}
                        </th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {items?.map(item => (
                    <tr
                        className="bg-white lg:hover:bg-blue-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
                        key={`col_tr_${item.id}`}>
                        {columns?.map(col => (
                            <td
                                className="w-full lg:w-auto p-3 text-blue-800 text-center border border-b block lg:table-cell relative lg:static"
                                key={`${item.id}_${col.title}`}>
                                <span className="lg:hidden absolute top-0 left-0 bg-blue-200 px-2 py-1 text-xs font-bold uppercase">
                                    {col.title}
                                </span>
                                {renderContent(col, item)}
                            </td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table>
    )
}

export default Table
