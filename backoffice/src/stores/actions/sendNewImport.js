import { ACTION_sendNewImport } from './types'

const sendNewImport = ({ rest }, data) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }

    var formattedData = []
    const columns = [
        'Mag',              // A
        'Codice',           // B
        'Nome',             // C
        'Descrizione',      // D
        'Codice M.P.',      // E
        'Nome M.P.',        // F
        'Diametro',         // G
        'MO',               // H
        'Data Creazione',   // I
        'Qta Ordinata',     // J
        'Qta Prodotta',     // K
        'Tempo Ciclo',      // L
        'Tempo Setup',      // M
        'ORE',              // N
        'DataTxt_1',        // O
        'DataPianificata_1',// P
        'DataOrdinePadre_1',// Q
        'MO Padre',         // R
        'Descrizione_2',    // S
        'Nome Oper.',       // T
        'FP1',              // U
        'FP2',              // V
        'FS1',              // W
        'FS2',              // X
        'FS3',              // Y
        'FS4',              // Z
        'FS5',              // AA
        'FS6',              // AB
        'FS7',              // AC
        'FS8',              // AD
        'FS9',              // AE
        'FS10',             // AF
        'FS11',             // AG
        'FS12',             // AH
        'FS13',             // AI
        'FS14',             // AJ
        'FS15',             // AK
        'FS16',             // AL
        'CRD ',             // AM
        'Cod. Cliente',     // AN
        'Rag. Sociale',     // AO
        'N. Oper.',         // AP
        'WC',               // AQ
        'Stato',            // AR
        'Stato Fase',       // AS
        'Data fine produzione',// AT
        'Data Fine Pian. OP',// AU
        'Data Ultima Mod.', // AV
        'ID Utente Ult. Mod.',// AW
        'Nome WC',          // AX
        'Testo',            // AY
        'Mat Prima',        // AZ
        'QOrdinata',        // BA
        'QImpegnata',       // BB
        'QDisponibile',     // BC
        'Codice Std/Sim/Spe',// BD
        'Descrizione Std/Sim/Spe',// BE
        'Test Ok/NOK',      // BF
        'Serie',            // BG
        'Tipologia',        // BH
        'Resp.',            // BI
        'Stampato',         // BJ
        'Reference Order',  // BK
    ]
    data.forEach((e, i) => {
        const obj = {}
        for (let i = 0; i < columns.length; i++) {
            const v = e[i]
                ? e[i].replace("'", '').replace(',00', '').trim()
                : ''

            const k = columns[i].replace("'", '').replace('"', '').trim()
            obj[k] = v
        }
        formattedData.push(obj)
    })
    console.log(formattedData[1])

    const request = rest.post(`mos/import`, { data: formattedData }, config)

    return dispatch({
        payload: request,
        type: ACTION_sendNewImport,
    })
}

export default sendNewImport
