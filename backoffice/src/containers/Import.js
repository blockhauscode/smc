import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { toast } from 'react-toastify'
import Papa from 'papaparse'
import { confirmAlert } from 'react-confirm-alert'
import { FilePicker } from 'react-file-picker-preview'
import { withContainer, connect } from '../services/container'
import LoggedNavbar from '../components/loggedNavbar'
import PageTitle from '../components/pageTitle'
import Button from '../components/button'
import Alert from '../components/alert'
import { sendNewImport, resetImport } from '../stores/actions'

export class ManageAreas extends Component {
    state = {
        data: null, // parsed data to be uploaded
        file: {},
        reset: {},
    }

    onChangeHandler = (file) => {
        this.setState({ file })
        Papa.parse(file, {
            complete: (results) => {
                const hasError = results.errors.length > 0
                this.setState({
                    data: hasError ? null : results.data,
                    error: hasError,
                })
            },
        })
    }

    resetInitialState() {
        this.setState({
            data: null,
            file: {},
            reset: {},
        })
    }

    uploadImportedData = async () => {
        try {
            await this.props.sendNewImport(this.state.data)
            toast.success('Import completato')
            this.resetInitialState()
        } catch (error) {
            toast.error('Something wrong')
        }
    }

    doResetImport = async () => {
        try {
            await this.props.resetImport(this.state.data)
            this.resetInitialState()
            toast.success('Import reset')
        } catch (error) {
            toast.error('Something wrong')
        }
    }

    alertConfirmResetImport = () => {
        confirmAlert({
            customUI: ({ onClose }) => (
                <Alert
                    onPositive={async () => {
                        await this.doResetImport()
                        onClose()
                    }}
                    onNegative={onClose}
                    title="Sei sicuro fare un reset del piano di lavoro?"
                />
            ),
        })
    }

    render() {
        const { file } = this.state
        return (
            <div>
                <LoggedNavbar
                    links={[{ name: 'Dashboard', to: '/dashboard' }]}
                />
                <PageTitle title="Import dati" />

                <form method="post" action="#">
                    <div className="">
                        <div className="flex flex-col m-10 text-center items-center">
                            <FilePicker
                                className="file-picker"
                                // extensions={['text/csv']}
                                maxSize={30}
                                buttonText="Fai upload del tuo CSV"
                                onChange={this.onChangeHandler}
                                onError={(error) => {
                                    alert("C'é stato un errore: " + error)
                                }}
                                onClear={() =>
                                    this.setState({ file: {}, data: null })
                                }
                                triggerReset={this.state.reset}>
                                <div className="input-button" type="button">
                                    The file picker
                                </div>
                            </FilePicker>
                            <div className="file-details">
                                <h3>Analisi file</h3>
                                <h4>Nome: {file.name}</h4>
                                <h4>
                                    Dimensione: {file.size}
                                    {file.size ? ' bytes' : null}
                                </h4>
                                <h4>Type: {file.type}</h4>
                            </div>
                        </div>
                    </div>
                </form>

                <div className="flex flex-row m-10 justify-end">
                    {this.state.data && (
                        <Button
                            isLoading={this.props.main.performingImport}
                            onClick={this.uploadImportedData}
                            text="Invia"
                            style="px-4 py-2 self-end"
                        />
                    )}
                </div>
                <div className="flex flex-row m-10 justify-end">
                    <Button
                        isLoading={this.props.main.performingResetImport}
                        onClick={this.alertConfirmResetImport}
                        text="Reset"
                        style="px-4 py-2 self-end"
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = ({ main }) => ({ main })

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            sendNewImport,
            resetImport,
        },
        dispatch,
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(ManageAreas))
