import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Loader from 'react-loader-spinner'

const Button = ({ onClick, text, style = '', icon, isLoading }) => {
    const defaultStyleSimple = 'btn btn-blue '
    const defaultStyleIcon =
        'tooltip hover:border w-10 h-10 hover:bg-blue-200 cursor-pointer border-blue-500 text-blue-500 flex items-center justify-center '

    const simpleBtn = (
        <div onClick={onClick} className={defaultStyleSimple + style}>
            {text}
        </div>
    )

    const iconBtn = (
        <div onClick={onClick} className={defaultStyleIcon + style}>
            <FontAwesomeIcon icon={icon} />
            <span className="tooltip-text -mt-10 bg-white">{text}</span>
        </div>
    )

    const loader = (
        <Loader type="ThreeDots" color="#00BFFF" height={30} width={30} />
    )
    const btn = icon ? iconBtn : simpleBtn
    return isLoading ? loader : btn
}

export default Button
