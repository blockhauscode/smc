import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Select from 'react-select'
import Input from '../../components/input'

const validate = (values) => {
    const errors = {}
    if (!values.name) {
        errors.name = 'Required'
    } else if (values.name.length > 20) {
        errors.name = 'Must be 20 characters or less'
    }
    return errors
}

export class MachineForm extends Component {
    constructor(props) {
        super(props)
    }

    onMachineFormButtonPress = () => {
        this.props.onSubmitForm()
    }

    renderReactDropdown = ({ input }) => {
        const options = this.props.areas?.map((x) => ({
            value: x.id,
            label: x.name,
        }))
        return (
            <div className="w-full max-w-screen-sm">
                <Select
                    {...input}
                    placeholder="Seleziona area"
                    onChange={(value) => input.onChange(value)}
                    onBlur={() => input.onBlur(input.value)}
                    options={options}
                />
            </div>
        )
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    placeholder="AX1234"
                    type="text"
                    name="code"
                    label="Code"
                    component={Input}
                />
                <Field
                    placeholder="0"
                    type="text"
                    name="f1"
                    label="Φ1"
                    component={Input}
                />
                <Field
                    placeholder="0"
                    type="text"
                    name="f2"
                    label="Φ2"
                    component={Input}
                />
                <Field
                    placeholder="0"
                    type="text"
                    name="f3"
                    label="Φ3"
                    component={Input}
                />
                <Field
                    placeholder="Descrizione"
                    type="text"
                    name="description"
                    label="Descrizione"
                    component={Input}
                />
                <Field
                    placeholder="Descrizione breve"
                    type="text"
                    name="shortDescription"
                    label="Descrizione breve"
                    component={Input}
                />
                <Field name="area" component={this.renderReactDropdown} />

                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onMachineFormButtonPress}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'MachineForm',
    validate,
})(MachineForm)
