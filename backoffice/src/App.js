import React from 'react'
import { ToastContainer } from 'react-toastify'
import configureStore from './stores/configureStore'
import { Provider } from 'react-redux'
import { ContainerProvider, Container } from './services/container'
import { Route, Switch } from 'react-router-dom'
import LoginRequiredRoute from './containers/LoginRequiredRoute'
import Login from './containers/Login'
import ManageAreas from './containers/ManageAreas'
import ManageMachines from './containers/ManageMachines'
import ManageWorkers from './containers/ManageWorkers'
import Planner from './containers/Planner'
import Dashboard from './containers/Dashboard'
import Import from './containers/Import'
import Unauthorized from './containers/Unauthorized'
import Settings from './containers/Settings'
import MessageCenter from './containers/MessageCenter'
import 'react-toastify/dist/ReactToastify.css'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'
import '../src/styles/import.css'
import { PersistGate } from 'redux-persist/integration/react'

const container = Container()
const { store, persistor } = configureStore()

export const appStore = store

const App = () => {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <ToastContainer />
                <ContainerProvider container={container}>
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <Route exact path="/login" component={Login} />
                        <LoginRequiredRoute
                            exact
                            path="/dashboard"
                            component={Dashboard}
                        />
                        <Route
                            exact
                            path="/unauthorized"
                            component={Unauthorized}
                        />
                        <LoginRequiredRoute
                            exact
                            path="/manageAreas"
                            component={ManageAreas}
                        />
                        <LoginRequiredRoute
                            exact
                            path="/manageMachines"
                            component={ManageMachines}
                        />
                        <LoginRequiredRoute
                            exact
                            path="/manageWorkers"
                            component={ManageWorkers}
                        />
                        <LoginRequiredRoute
                            exact
                            path="/planner"
                            component={Planner}
                        />
                        <LoginRequiredRoute
                            exact
                            path="/import"
                            component={Import}
                        />
                        <LoginRequiredRoute
                            exact
                            path="/settings"
                            component={Settings}
                        />
                        <LoginRequiredRoute
                            exact
                            path="/messages"
                            component={MessageCenter}
                        />
                    </Switch>
                </ContainerProvider>
            </PersistGate>
        </Provider>
    )
}

export default App
