import { ACTION_createAdmin } from './types'

const createAdmin = ({ rest }, payload) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
        },
    }
    const { firstName, lastName, email } = getState().form.AdminForm.values

    const request = rest.post(
        `accounts`,
        {
            firstName,
            lastName,
            email,
            emailVerified: true,
            role: 'admin',
            password: 'smc',
        },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_createAdmin,
    })
}

export default createAdmin
