// import { ACTION_login } from '../actions/types'
// import { ACTION_logout } from '../actions/types'

const INITIAL_STATE = {
    isAttemptingLogin: false,
    isAttemptingLogout: false,
    isAttemptingSignup: false,
    token: null,
    settings: {
        cl: 15,
    },
}

/**
 * @param {Object} state - Default aplication state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
    // TODO
    // Import ACTION_logout ACTION_login constants not working
    const ACTION_logout = 'ACTION_logout'
    const ACTION_login = 'ACTION_login'
    // END

    const { type, payload } = action
    switch (type) {
        case `ACTION_login_PENDING`:
            return {
                ...state,
                isAttemptingLogin: true,
            }
        case `ACTION_login_FULFILLED`:
            return {
                ...state,
                isAttemptingLogin: false,
                token: payload.data.id,
                ...payload.data.user,
            }
        case `ACTION_login_REJECTED`:
            return {
                ...state,
                isAttemptingLogin: false,
            }
        case `ACTION_logout_PENDING`:
            return {
                ...state,
                isAttemptingLogout: true,
            }
        case `ACTION_logout_FULFILLED`:
            return {
                ...INITIAL_STATE,
            }
        case `ACTION_Logout_REJECTED`:
            return {
                ...state,
                isAttemptingLogout: false,
            }
        case `ACTION_fetchSettings_FULFILLED`:
            return {
                ...state,
                settings: payload.data[0],
            }
        case `ACTION_UpdateSettings_FULFILLED`:
            return {
                ...state,
                settings: payload.data,
            }
        default:
            return state
    }
}
