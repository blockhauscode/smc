import { createTheming } from '@callstack/react-theme-provider'

export const themes = {
    1: {
        lightColor: '#F6BD45',
        darkColor: '#223457',
        accent: '#DE1725',
    },
    2: {
        lightColor: '#f39200',
        darkColor: '#522c00',
        accent: '#DE1725',
    },
    3: {
        lightColor: '#8ACBC1',
        darkColor: '#553D38',
        accent: '#DE1725',
    },
}

const { ThemeProvider, withTheme } = createTheming(themes)

export { ThemeProvider, withTheme }
