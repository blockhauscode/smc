import { ACTION_loadAreasWithMachines } from './types'

const loadAreasWithMachines = ({ rest }) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
            filter: { include: [{ machines: ['workers'] }] },
        },
    }
    const request = rest.get(`areas`, config)

    return dispatch({
        payload: request,
        type: ACTION_loadAreasWithMachines,
    })
}

export default loadAreasWithMachines
