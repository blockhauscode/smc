import React, { Component } from 'react'

import { bindActionCreators } from 'redux'
import Modal from 'react-modal'
import { withContainer, connect } from '../services/container'
import DepartmentForm from '../components/forms/DepartmentForm'
import AreaForm from '../components/forms/AreaForm'
import LoggedNavbar from '../components/loggedNavbar'
import PageTitle from '../components/pageTitle'
import Button from '../components/button'
import Box from '../components/box'
import Alert from '../components/alert'
import Table from '../components/table'
import {
    faEdit,
    faTrash,
    faPlus,
    faTimes,
} from '@fortawesome/free-solid-svg-icons'
import { confirmAlert } from 'react-confirm-alert'

import {
    createArea,
    editArea,
    deleteArea,
    deleteDepartment,
    editDepartment,
    createDepartment,
    loadDepartments,
} from '../stores/actions'

export class ManageAreas extends Component {
    state = {
        modalDepartment: false, // modal edit and create department open or close
        modalArea: false, // modal edit and create area open or close
        selectedDepartment: null, // when we manage an area, we have to keep track of the related department
        departmentEditMode: null, // department that is under edit mode
        areaEditMode: null, // area that is under edit mode
    }

    componentDidMount() {
        this.props.loadDepartments()
    }

    openModalDep(dep) {
        this.setState({
            modalDepartment: true,
            departmentEditMode: dep,
        })
    }

    closeModalDep() {
        this.setState({
            modalDepartment: false,
            departmentEditMode: null,
        })
    }

    openModalArea(dep, area) {
        this.setState({
            modalArea: true,
            selectedDepartment: dep,
            areaEditMode: area,
        })
    }

    closeModalArea() {
        this.setState({
            modalArea: false,
            selectedDepartment: null,
            areaEditMode: null,
        })
    }

    onDepartmentFormSubmit = async () => {
        if (this.state.departmentEditMode) {
            await this.props.editDepartment(this.state.departmentEditMode.id)
        } else {
            await this.props.createDepartment()
        }
        this.closeModalDep()
        this.props.loadDepartments()
    }

    onAreaFormSubmit = async () => {
        if (this.state.areaEditMode) {
            await this.props.editArea(this.state.areaEditMode.id)
        } else {
            await this.props.createArea(this.state.selectedDepartment.id)
        }
        this.closeModalArea()
        this.props.loadDepartments()
    }

    onDeleteArea = (areaId) => this.props.deleteArea(areaId)
    onDeleteDepartment = (departmentId) =>
        this.props.deleteDepartment(departmentId)

    tryToDeleteDepartment = async (departmentId, onClose) => {
        try {
            await this.onDeleteDepartment(departmentId)
            onClose()
            this.props.loadDepartments()
        } catch (error) {
            onClose()
            this.alertCantDelete()
        }
    }

    alertCantDelete = () => {
        confirmAlert({
            customUI: ({ onClose }) => (
                <Alert
                    onPositive={onClose}
                    title="Non é possibile eliminare un department contenente delle aree"
                    positiveText="Ok"
                />
            ),
        })
    }

    alertConfirmDeleteDepartment = (department) => {
        confirmAlert({
            customUI: ({ onClose }) => (
                <Alert
                    onPositive={() =>
                        this.tryToDeleteDepartment(department.id, onClose)
                    }
                    onNegative={onClose}
                    title="Sei sicuro di voler eliminare questo dipartimento?"
                />
            ),
        })
    }

    alertConfirmDeleteArea = (area) => {
        confirmAlert({
            customUI: ({ onClose }) => (
                <Alert
                    onPositive={async () => {
                        await this.onDeleteArea(area.id)
                        onClose()
                        this.props.loadDepartments()
                    }}
                    onNegative={onClose}
                    title="Sei sicuro di voler eliminare questa area?"
                />
            ),
        })
    }

    render() {
        const { departments } = this.props.main
        const { selectedDepartment } = this.state
        const actionsDep = (dep) => (
            <div className="flex justify-end">
                <Button
                    onClick={() => this.openModalDep(dep)}
                    text="Modifica Dipartimento"
                    icon={faEdit}
                    style="px-4 self-end ml-2"
                />
                <Button
                    onClick={() => this.alertConfirmDeleteDepartment(dep)}
                    text="Elimina Dipartimento"
                    icon={faTrash}
                    style="px-4 self-end ml-2"
                />
            </div>
        )

        return (
            <div>
                <LoggedNavbar
                    links={[{ name: 'Dashboard', to: '/dashboard' }]}
                />
                <PageTitle title="Gestione Aree" />
                {departments?.map((dep, i) => (
                    <Box
                        title={`Dipartimento ${dep.name}`}
                        key={`dep_${i}`}
                        actions={actionsDep(dep)}>
                        <Table
                            columns={[
                                {
                                    title: 'Area',
                                    contentType: 'itemProp',
                                    key: 'name',
                                },
                                {
                                    title: 'Macchine',
                                    contentType: 'itemProp',
                                    key: 'machines.length',
                                },
                                {
                                    title: 'Azioni',
                                    contentType: 'actions',
                                    actions: [
                                        {
                                            icon: faEdit,
                                            name: 'Modifica',
                                            onClick: (area) =>
                                                this.openModalArea(dep, area),
                                        },
                                        {
                                            icon: faTrash,
                                            name: 'Elimina',
                                            onClick: this
                                                .alertConfirmDeleteArea,
                                        },
                                    ],
                                },
                            ]}
                            items={dep.areas.sort((x, y) =>
                                x.name < y.name ? -1 : 1,
                            )}></Table>
                        <div className="flex justify-end">
                            <Button
                                onClick={() => this.openModalArea(dep)}
                                text="Crea Nuova Area"
                                style="px-4 self-end ml-2"
                            />
                        </div>
                    </Box>
                ))}
                <div className="flex flex-col m-10">
                    <Button
                        onClick={() => this.openModalDep()}
                        text="Crea Nuovo Dipartimento"
                        style="px-4 py-2 self-end"
                    />
                </div>

                {/* Create new department */}
                <Modal
                    isOpen={this.state.modalDepartment}
                    onRequestClose={() => this.closeModalDep()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalDep()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle
                        title={`${
                            this.state.departmentEditMode
                                ? 'Modifica dipartimento'
                                : 'Crea un nuovo dipartimento'
                        }`}
                    />
                    <DepartmentForm
                        onSubmitForm={this.onDepartmentFormSubmit}
                        initialValues={this.state.departmentEditMode}
                        actionText={`${
                            this.state.departmentEditMode
                                ? 'Salva Modifiche'
                                : 'Crea Dipartimento'
                        }`}
                    />
                </Modal>

                {/* Create new area */}
                <Modal
                    isOpen={this.state.modalArea}
                    onRequestClose={() => this.closeModalArea()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalArea()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle
                        title={`${
                            this.state.areaEditMode
                                ? 'Modifica questa area appartenente a '
                                : 'Crea una nuova area in'
                        }  ${selectedDepartment && selectedDepartment.name}`}
                    />
                    <AreaForm
                        onSubmitForm={this.onAreaFormSubmit}
                        initialValues={this.state.areaEditMode}
                        actionText={`${
                            this.state.areaEditMode
                                ? 'Salva Modifiche'
                                : 'Crea Area'
                        }`}
                    />
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = ({ main }) => ({ main })

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            createArea,
            editArea,
            deleteArea,
            deleteDepartment,
            editDepartment,
            createDepartment,
            loadDepartments,
        },
        dispatch,
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(ManageAreas))
