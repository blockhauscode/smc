import { ACTION_createInfo } from './types'

const createInfo = ({ rest }, reference) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const { info } = getState().form.InfoCodeForm.values
    const request = rest.post(
        `infos`,
        {
            text: info,
            reference
        },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_createInfo,
    })
}

export default createInfo
