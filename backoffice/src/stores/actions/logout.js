import { ACTION_logout } from './types'

const logout = ({ rest }, payload) => (dispatch, getState) => {
    const params = { params: { access_token: getState().user.token } }
    const accountOrWorker =
        getState().user.role === 'worker' ? 'workers' : 'accounts'
    const request = rest.post(`${accountOrWorker}/logout`, null, params)

    return dispatch({
        payload: request,
        type: ACTION_logout,
    })
}

export default logout
