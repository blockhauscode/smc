import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../../components/input'

const validate = values => {
    const errors = {}
    if (!values.name) {
        errors.name = 'Required'
    } else if (values.name.length > 20) {
        errors.name = 'Must be 20 characters or less'
    }
    return errors
}

export class DepartmentForm extends Component {
    constructor(props) {
        super(props)
    }

    onDepartmentFormButtonPress = () => {
        this.props.onSubmitForm()
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    placeholder="Produzione"
                    type="text"
                    name="name"
                    label="Nome"
                    component={Input}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onDepartmentFormButtonPress}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'DepartmentForm',
    validate,
})(DepartmentForm)
