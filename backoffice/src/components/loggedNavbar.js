import React from 'react'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { Redirect } from 'react-router-dom'
import Button from '../components/button'
import { logout } from '../stores/actions'
import { connect } from '../services/container'
import logo from '../assets/logo.jpg'

class LoggedNavbar extends React.Component {
    state = {
        redirectToLogin: false,
    }

    onLogoutSubmit = async () => {
        try {
            await this.props.logout()
            this.setState(() => ({
                redirectToLogin: true,
            }))
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        const { links } = this.props
        const { redirectToLogin } = this.state
        if (redirectToLogin) {
            return <Redirect to="/login" />
        }
        return (
            <div className="bg-white h-10 sm:h-20 border-b border-blue-400 flex items-center justify-between">
                {links &&
                    this.props.links?.map((link, i) => (
                        <Link to={link.to} key={`link_${i}`}>
                            <span className="text-blue mx-8 hover:text-blue-500">
                                {link.name}
                            </span>
                        </Link>
                    ))}
                {!links && (
                    <Button
                        onClick={this.onLogoutSubmit}
                        text="Logout"
                        style="border-0 ml-4"
                    />
                )}
                <img className="h-8 sm:h-12 mx-8" src={logo}></img>
            </div>
        )
    }
}

const mapStateToProps = ({ main }) => ({ main })

const mapDispatchToProps = dispatch => bindActionCreators({ logout }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(LoggedNavbar)
