import { ACTION_loadDepartments } from './types'

const loadDepartments = ({ rest }) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
            filter: { include: [{ areas: ['machines'] }] },
        },
    }
    const request = rest.get(`departments`, config)

    return dispatch({
        payload: request,
        type: ACTION_loadDepartments,
    })
}

export default loadDepartments
