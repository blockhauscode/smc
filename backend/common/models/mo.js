'use strict'

const moment = require('moment')
const app = require('../../server/server')
const MO = 'MO'
const MO_PADRE = 'MO Padre'
const NAME = 'Nome'
const DESCRIPTION = 'Descrizione'
const STATO_FASE = 'Stato Fase'
const N_OPER = 'N. Oper.'
const DATA_TXT = 'DataPianificata_1'
const WC = 'WC'
const QUANTITY = 'Qta Ordinata'
const CODE = 'Codice'
const DIAMETRO = 'Diametro'
const NAME_OPER = 'Nome Oper.'
const FP1 = 'FP1'
const CODICEMP = 'Codice M.P.'
const NOMEMP = 'Nome M.P.'

module.exports = function (Mo) {
    const getId = (mo) => `${mo[MO]}_${mo[N_OPER]}_${mo[STATO_FASE]}`

    let createCount = 0
    Mo.import = async function (data) {
        const { Settings } = app.models
        // skip the first element
        for (let i = 0; i < data.length - 1; i++) {
            const el = data[i]
            const newDate = moment(el[DATA_TXT], 'DD/MM/YYYY').toISOString()
            const mo = await Mo.findById(getId(el))
            
            const str = JSON.stringify(el).toUpperCase()
            const isTaglio =
                str.indexOf('SHARK') >= 0 ||
                str.indexOf('TAGLI CQ2') >= 0 ||
                str.indexOf('TAGLI MANUALI') >= 0 || 
                str.indexOf('ELUMATEC') >= 0 
            const isContoLavoro = str.indexOf('CONTO LAVORO') >= 0
            const isAnodica = str.indexOf('ANODICA') >= 0

            if (mo) {
                // mo already exists, check date and eventually edit
                const oldDate = [...mo.date].pop()
                const nextDateArray = newDate !== oldDate ? [...mo.date, newDate] : newDate
                mo.updateAttributes({
                    date: nextDateArray,
                    dateT: newDate,
                    quantity: el[QUANTITY],
                    moPadre: el[MO_PADRE],
                    mo: el[MO],
                    name: el[NAME],
                    description: el[DESCRIPTION],
                    statoFase: el[N_OPER],
                    nOper: el[STATO_FASE],
                    machineCode: el[WC],
                    code: el[CODE],
                    diametro: el[DIAMETRO],
                    nameOper: el[NAME_OPER],
                    CODICEMP: el[CODICEMP],
                    NOMEMP: el[NOMEMP],
                    NomeOper: el['Nome Oper.'],
                    FP1: el[FP1],
                    FS1: el['FS1'],
                    FS2: el['FS2'],
                    FS3: el['FS3'],
                    FS4: el['FS4'],
                    FS5: el['FS5'],
                    FS6: el['FS6'],
                    FS7: el['FS7'],
                    FS8: el['FS8'],
                    FS9: el['FS9'],
                    FS10: el['FS12'],
                    FS11: el['FS11'],
                    FS12: el['FS12'],
                    FS13: el['FS13'],
                    FS14: el['FS14'],
                    FS15: el['FS15'],
                    FS16: el['FS16'],
                    statusText: '',
                    isContoLavoro,
                    isTaglio,
                    isAnodica,
                    note: '',
                })

            } else {
                try {
                    await Mo.create({
                        customId: getId(el),
                        moPadre: el[MO_PADRE],
                        mo: el[MO],
                        name: el[NAME],
                        description: el[DESCRIPTION],
                        statoFase: el[N_OPER],
                        nOper: el[STATO_FASE],
                        machineCode: el[WC],
                        date: [newDate],
                        dateT: newDate,
                        quantity: el[QUANTITY],
                        code: el[CODE],
                        diametro: el[DIAMETRO],
                        nameOper: el[NAME_OPER],
                        CODICEMP: el[CODICEMP],
                        NOMEMP: el[NOMEMP],
                        NomeOper: el['Nome Oper.'],
                        FP1: el[FP1],
                        FS1: el['FS1'],
                        FS2: el['FS2'],
                        FS3: el['FS3'],
                        FS4: el['FS4'],
                        FS5: el['FS5'],
                        FS6: el['FS6'],
                        FS7: el['FS7'],
                        FS8: el['FS8'],
                        FS9: el['FS9'],
                        FS10: el['FS12'],
                        FS11: el['FS11'],
                        FS12: el['FS12'],
                        FS13: el['FS13'],
                        FS14: el['FS14'],
                        FS15: el['FS15'],
                        FS16: el['FS16'],
                        statusText: '',
                        isContoLavoro,
                        isTaglio,
                        isAnodica,
                        note: '',
                    })
                    createCount++
                } catch (error) {
                    console.log(error)
                }
            }
        }
        Settings.setLastImport()
        return `Created ${createCount} mo`
    }

    Mo.resetImport = async function (data) {
        return Mo.destroyAll({})
    }

    // // metodo di istanza
    // Mo.afterRemote('prototype.import', () => {
    // })
    // // metodo di classe
    // Mo.afterRemote('create', () => {
    // })
}

// OK
// {  "where":{  "dateT": { "gt": 1583766909822 } }  ,"limit": 10, "skip": 0, "order": "dateT ASC"}
