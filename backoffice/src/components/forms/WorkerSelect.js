import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Select from 'react-select'

export class WorkerSelect extends Component {
    constructor(props) {
        super(props)
    }

    renderReactDropdown = ({ input }) => {
        const options = this.props.workers?.map((x) => ({
            value: x.id,
            label: `${x.firstName} ${x.lastName}`,
        }))
        return (
            <div className="w-full max-w-screen-sm mt-4">
                <Select
                    {...input}
                    placeholder="Seleziona operatore"
                    onChange={(value) => input.onChange(value)}
                    onBlur={() => input.onBlur(input.value)}
                    options={options}
                    isClearable={true}
                />
            </div>
        )
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field name="worker" component={this.renderReactDropdown} />
            </div>
        )
    }
}

export default reduxForm({
    form: 'WorkerSelect',
})(WorkerSelect)
