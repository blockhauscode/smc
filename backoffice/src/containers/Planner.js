import React, { Component } from 'react'
import moment from 'moment'
import { bindActionCreators } from 'redux'
import { withContainer, connect } from '../services/container'
import DataTable from 'react-data-table-component'
import DatePicker from 'react-datepicker'
import Modal from 'react-modal'
import LoggedNavbar from '../components/loggedNavbar'
import PageTitle from '../components/pageTitle'
import { loadMos, editNote, loadWorkersWithMachines, fetchInfo, createInfo, editInfo } from '../stores/actions'
import 'react-datepicker/dist/react-datepicker.css'
import Button from '../components/button'
import NoteForm from '../components/forms/NoteForm'
import InfoCodeForm from '../components/forms/InfoCodeForm'
import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons'
import WorkerSelect from '../components/forms/WorkerSelect'

export class Planner extends Component {
    state = {
        startDate: new Date(),
        endDate: new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000), // a week
        modalNote: false,
        moEditMode: null,
        modalText: false,
        search: '',
    }
    componentDidMount() {
        this.props.loadMos(this.state.startDate)
        this.props.loadWorkersWithMachines()
    }

    getColumns() {
        return [
            {
                name: 'Data',
                selector: 'dateT',
                sortable: true,
                format: (row, index) => moment(row.dateT).format('DD/MM/YYYY'),
            },
            {
                name: 'Macchina',
                selector: 'nameOper',
                sortable: true,
            },
            {
                name: 'Mo',
                selector: 'mo',
                sortable: true,
            },
            {
                name: 'Codice',
                cell: (row) => (
                    <div
                        style={{ cursor: 'pointer' }}
                        onClick={() => this.openModalCode(row.code)}>
                        {row.code}
                    </div>
                ),
            },

            {
                name: 'Fase',
                selector: 'statoFase',
                sortable: false,
            },
            {
                name: 'Quantitá',
                selector: 'quantity',
            },
            {
                name: 'Descrizione',
                selector: 'description',
            },
            {
                name: 'Note',
                cell: (row) => (
                    <div
                        style={{ cursor: 'pointer' }}
                        onClick={() => this.openModalText(row.note)}>
                        {row.note.length > 40 ? `${row.note.substring(0,40)}...` : row.note}
                    </div>
                ),
            },
            {
                sortable: false,
                cell: (row) => (
                    <Button
                        onClick={() => this.openModalNote(row)}
                        icon={faEdit}
                        text="Edit"
                        style="px-4 py-2 self-end"></Button>
                ),
            },
        ]
    }

    openModalText(text) {
        this.setState({
            modalText: text,
        })
    }

    closeModalText() {
        this.setState({
            modalText: false,
        })
    }

    openModalNote(mo) {
        this.setState({
            modalNote: true,
            moEditMode: mo,
        })
    }

    closeModalNote() {
        this.setState({
            modalNote: false,
            moEditMode: null,
        })
    }

    openModalCode(code) {
        console.log(code)
        this.setState({
            modalCode: true,
            codeOpen: code,
        })
    }

    closeModalCode() {
        this.setState({
            modalCode: false,
            codeOpen: null,
        })
    }

    onNoteFormSubmit = async () => {
        await this.props.editNote(this.state.moEditMode.customId)
        this.closeModalNote()
        this.props.loadMos(this.state.startDate)
    }

    onCodeFormSubmit = async (isNewCode) => {
        console.log('isNewCode', isNewCode)
        console.log(this.state.codeOpen)
        if(isNewCode){
            this.props.createInfo(this.state.codeOpen)
        }else {
            this.props.editInfo(this.state.codeOpen)
        }
        this.closeModalCode()
    }

    handleChange(e) {
        this.setState({
            search: e.target.value,
        })
    }

    render() {
        return (
            <div>
                <LoggedNavbar
                    links={[{ name: 'Dashboard', to: '/dashboard' }]}
                />
                <PageTitle title="Planner Manuale" />
                <WorkerSelect
                    workers={this.props.main.workers.sort((x, y) =>
                        x.firstName < y.firstName ? -1 : 1,
                    )}></WorkerSelect>
                <div className="flex flex-row mh-4 text-center items-center justify-center">
                    <label
                        style={{
                            position: 'relative',
                            transform: 'translateX(40px)',
                            color: 'gray',
                        }}>
                        Dal
                    </label>
                    <DatePicker
                        style={{
                            margin: 10,
                            display: 'flex',
                            justifyContent: 'center',
                        }}
                        showPopperArrow={false}
                        dateFormat="dd/MM/yyyy"
                        selected={this.state.startDate}
                        onChange={(date) => {
                            // this.props.loadMos(date)
                            this.setState({
                                startDate: date,
                            })
                        }}
                    />
                    <label
                        style={{
                            position: 'relative',
                            transform: 'translateX(30px)',
                            color: 'gray',
                        }}>
                        Al
                    </label>
                    <DatePicker
                        style={{
                            margin: 10,
                            display: 'flex',
                            justifyContent: 'center',
                        }}
                        showPopperArrow={false}
                        dateFormat="dd/MM/yyyy"
                        selected={this.state.endDate}
                        onChange={(date) => {
                            this.setState({
                                endDate: date,
                            })
                        }}
                        minDate={this.state.startDate}
                    />

                    <Button
                        onClick={() =>
                            this.props.loadMos(
                                this.state.startDate,
                                this.state.endDate,
                            )
                        }
                        text="Cerca"
                        style="px-4 py-2 ml-2 self-end"
                    />
                </div>
                <div className="flex flex-row mh-2 text-center items-center justify-center">
                    <div className="m-2">
                        <label className="p-2 text-center inline-block">
                            Filtra per codice
                        </label>

                        <input
                            className="border-blue-500 border  m-2 p-2 text-sm outline-none appearance-none"
                            placeholder="Codice"
                            onChange={(e) => this.handleChange(e)}
                            type="text"
                            name="text"
                        />
                    </div>
                </div>
                <DataTable
                    columns={this.getColumns()}
                    data={this.props.main.mos?.filter((x) =>
                        x.code.startsWith(this.state.search.trim()),
                    )}
                    pagination
                    paginationPerPage={50}
                    paginationRowsPerPageOptions={[50, 100, 300]}
                    paginationComponentOptions={{
                        rowsPerPageText: 'Righe per pagina:',
                        rangeSeparatorText: 'di',
                        noRowsPerPage: false,
                    }}
                />

                {/* edit note */}
                <Modal
                    isOpen={!!this.state.modalNote}
                    onRequestClose={() => this.closeModalNote()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalNote()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle title={'Modifica questa info MO'} />
                    <NoteForm
                        onSubmitForm={this.onNoteFormSubmit}
                        initialValues={this.state.moEditMode}
                        actionText={'Salva Modifiche'}
                    />
                </Modal>
                <Modal
                    isOpen={!!this.state.modalText}
                    onRequestClose={() => this.closeModalText()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalText()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle title={'Nota'} />
                    <div className="flex flex-row p-4 text-center items-center justify-center">
                        {this.state.modalText}
                    </div>
                </Modal>
                <Modal
                    isOpen={!!this.state.modalCode}
                    onRequestClose={() => this.closeModalCode()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalCode()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle title={`Informazioni codice`} />
                    <InfoCodeForm
                        onSubmitForm={this.onCodeFormSubmit}
                        fetchInfo={this.props.fetchInfo}
                        actionText={'Salva'}
                        code={this.state.codeOpen}
                    />
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = ({ main }) => ({ main })

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            loadMos,
            editNote,
            loadWorkersWithMachines,
            fetchInfo,
            createInfo,
            editInfo
        },
        dispatch,
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(Planner))
