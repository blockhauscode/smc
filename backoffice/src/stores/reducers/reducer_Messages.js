import { ACTION_fetchMessages, ACTION_editMessage } from '../actions/types'
import moment from 'moment'

const INITIAL_STATE = {
    sent: [],
    received: [],
    lastUpdate: null,
    selectedMessage: null,
    lastUpdate: null,
}

/**
 * @param {Object} state - Default aplication state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action
    console.log(action)
    switch (type) {
        case `${ACTION_fetchMessages}_PENDING`:
            return {
                ...state,
                loadingMessages: true,
            }
        case `${ACTION_fetchMessages}_FULFILLED`:
            const userId =
                action.payload.config.params.filter.where.or[0].receiverId
            return {
                ...state,
                loadingMessages: false,
                received: payload.data
                    ? [
                          ...payload.data.filter(
                              (x) => x.receiverId === userId,
                          ),
                          ...state.received,
                      ]
                    : [...state.received],
                sent: payload.data
                    ? [
                          ...payload.data.filter((x) => x.senderId === userId),
                          ...state.sent,
                      ]
                    : [...state.sent],
                lastUpdate: moment(),
            }
        case `${ACTION_fetchMessages}_REJECTED`:
            return {
                ...state,
                loadingMessages: false,
            }
        case `${ACTION_editMessage}_FULFILLED`:
            return {
                ...INITIAL_STATE,
            }
        case `ACTION_login_FULFILLED`:
            return {
                ...INITIAL_STATE,
            }
        default:
            return state
    }
}
