import { ACTION_fetchInfo } from './types'

const fetchInfo = ({ rest }, code) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
        },
    }

    const request = rest.get(`/infos/${code}`, config)

    return dispatch({
        payload: request,
        type: ACTION_fetchInfo,
    })
}

export default fetchInfo
