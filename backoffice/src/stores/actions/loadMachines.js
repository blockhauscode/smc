import { ACTION_loadMachines } from './types'

const loadMachines = ({ rest }) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
            filter: { include: '' },
        },
    }
    const request = rest.get(`machines`, config)

    return dispatch({
        payload: request,
        type: ACTION_loadMachines,
    })
}

export default loadMachines
