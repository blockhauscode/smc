import { ACTION_editDepartment } from './types'

const editDepartment = ({ rest }, departmentId) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }
    const { name } = getState().form.DepartmentForm.values
    const request = rest.put(`departments/${departmentId}`, { name }, config)

    return dispatch({
        payload: request,
        type: ACTION_editDepartment,
    })
}

export default editDepartment
