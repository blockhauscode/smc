import { ACTION_createWorker } from './types'

const createWorker = ({ rest }, payload) => (dispatch, getState) => {
    
    const config = {
        params: {
            access_token: getState().user.token,
        },
    }
    
    const {
        firstName,
        lastName,
        email,
        machines,
        folder,
    } = getState().form.WorkerForm.values

    const request = rest.post(
        `workers`,
        {
            firstName,
            lastName,
            email,
            role: 'worker',
            password: 'smc',
            folder,
            machines: machines?.map(x => x.value),
        },
        config,
    )

    return dispatch({
        payload: request,
        type: ACTION_createWorker,
    })
}

export default createWorker
