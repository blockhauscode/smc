import React from 'react'
import { bindActionCreators } from 'redux'
import { Redirect, Route } from 'react-router-dom'
import { connect } from '../services/container'

const LoginRequiredRoute = ({ component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props => {
                const isAdmin = rest.user.role === 'admin'
                const isLogged = !!rest.user.token
                const hasFullAccess = isAdmin && isLogged
                let redirectPath = '/login/'
                if (isLogged && !isAdmin) {
                    redirectPath = '/unauthorized'
                }
                return hasFullAccess ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: redirectPath,
                            state: { from: props.location },
                        }}
                    />
                )
            }}
        />
    )
}

const mapStateToProps = ({ user }) => ({ user })

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(LoginRequiredRoute)
