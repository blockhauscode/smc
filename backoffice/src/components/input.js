import React from 'react'

const INput = (field) => {
    return (
        <div className="m-4">
            <label className="p-2 w-24 text-center inline-block">
                {field.label}
            </label>
            {field.isArea ? (
                <textarea
                    className="border-blue-500 border p-2 text-sm w-full outline-none appearance-none"
                    {...field.input}
                    placeholder={field.placeholder}
                    type={field.type}
                />
            ) : (
                <input
                    className="border-blue-500 border p-2 text-sm outline-none appearance-none"
                    {...field.input}
                    placeholder={field.placeholder}
                    type={field.type}
                />
            )}
        </div>
    )
}

export default INput
