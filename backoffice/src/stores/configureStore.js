import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import promise from 'redux-promise-middleware'
import thunk from 'redux-thunk'
import allReducers from './reducers'

const createStoreWithMiddleware = composeWithDevTools(
    applyMiddleware(promise, thunk),
)(createStore)

const persistConfig = {
    key: 'user',
    storage,
    whitelist: ['user'],
}

const persistedReducer = persistReducer(persistConfig, allReducers)

export default () => {
    let store = createStoreWithMiddleware(persistedReducer)
    let persistor = persistStore(store)
    return { store, persistor }
}
