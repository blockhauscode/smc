import { ACTION_login } from './types'

const login = ({ rest }, payload) => async (dispatch, getState) => {
    const { password, email } = getState().form.Login.values
    const params = { params: { include: 'user' } }

    const { data } = await rest.get(`workers`, {
        params: { filter: { where: { email } } },
    })
    const isWorker = data.length > 0

    const request = rest.post(
        `${isWorker ? 'workers' : 'accounts'}/login`,
        { password, email },
        params,
    )

    return dispatch({
        payload: request,
        type: ACTION_login,
    })
}

export default login
