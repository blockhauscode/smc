import { ACTION_resetImport } from './types'

const sendNewImport = ({ rest }) => (dispatch, getState) => {
    const config = { params: { access_token: getState().user.token } }

    const request = rest.post(`mos/resetImport`, null, config)

    return dispatch({
        payload: request,
        type: ACTION_resetImport,
    })
}

export default sendNewImport
