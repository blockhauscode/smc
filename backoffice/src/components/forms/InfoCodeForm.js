import React, { Component } from 'react'
import { reduxForm, Field, initialize } from 'redux-form'
import Input from '../input'

export class InfoCodeForm extends Component {
    state = {
        isNewCode: false
    }

    constructor(props) {
        super(props)
    }

    async componentDidMount() {
        const res = await this.props.fetchInfo(this.props.code)
        if(res.value?.response?.data?.error.status === 404){
            // code not found
            this.setState({isNewCode: true})
        }
        if(res?.value?.data){
            const {text} = res?.value?.data
            const initialValues = { info: text };
            this.props.dispatch(initialize(
                'InfoCodeForm',
                initialValues,
              ));
        }
    }

    onInfoCodeFormButtonPress = () => {
        this.props.onSubmitForm(this.state.isNewCode)
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    name="info"
                    component={(props) => (
                        <Input
                            {...props}
                            isArea={true}
                            type="text"
                            name="info"
                            label="Info Codice"></Input>
                    )}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onInfoCodeFormButtonPress}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'InfoCodeForm',
})(InfoCodeForm)
