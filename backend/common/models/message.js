'use strict'
const app = require('../../server/server')

module.exports = function (Message) {
    Message.multiSend = async function (messages) {
        for (let i = 0; i < messages.length; i++) {
            const {
                subject,
                text,
                receiverId,
                senderId,
                senderType,
                receiverType,
            } = messages[i]
            try {
                await Message.create({
                    subject,
                    text,
                    receiverId,
                    senderId,
                    senderType,
                    receiverType,
                })
            } catch (error) {
                console.log(error)
            }
        }
        return `Messages sent`
    }

    Message.observe('after save', (ctx, next) => {
        const { Email } = app.models

        const { subject, text, senderType } = ctx.instance
        const isPasswordUpdate = subject === 'Cambio password'
        if (ctx.isNewInstance && senderType === 'worker' && !isPasswordUpdate) {
            Email.send(
                {
                    to: 'operatore.smc@gmail.com',
                    from: 'operatore.smc@gmail.com',
                    subject,
                    text,
                },
                function (err, mail) {
                    console.log('email sent!')
                    console.log(err)
                },
            )
        }
        next()
    })
}
