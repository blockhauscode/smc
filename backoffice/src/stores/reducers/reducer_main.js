import {
    ACTION_loadDepartments,
    ACTION_loadAreasWithMachines,
    ACTION_loadWorkersWithMachines,
    ACTION_loadMachines,
    ACTION_loadMos,
    ACTION_sendNewImport,
    ACTION_resetImport,
} from '../actions/types'

const INITIAL_STATE = {
    departments: [],
    areas: [],
    workers: [],
    loadingDepartments: false,
    performingImport: false,
    performingResetImport: false,
}

/**
 * @param {Object} state - Default aplication state
 * @param {Object} action - Action from action creator
 * @returns {Object} New state
 */
export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action
    switch (type) {
        case `${ACTION_loadDepartments}_PENDING`:
            return {
                ...state,
                loadingDepartments: true,
                departments: [],
            }
        case `${ACTION_loadDepartments}_FULFILLED`:
            return {
                ...state,
                loadingDepartments: false,
                departments: payload.data,
            }
        case `${ACTION_loadDepartments}_REJECTED`:
            return {
                ...state,
                loadingDepartments: false,
                departments: [],
            }
        case `${ACTION_loadAreasWithMachines}_PENDING`:
            return {
                ...state,
                loadingDepartments: true,
                areas: [],
            }
        case `${ACTION_loadAreasWithMachines}_FULFILLED`:
            return {
                ...state,
                loadingDepartments: false,
                areas: payload.data,
            }
        case `${ACTION_loadAreasWithMachines}_REJECTED`:
            return {
                ...state,
                loadingDepartments: false,
                areas: [],
            }
        case `${ACTION_loadWorkersWithMachines}_PENDING`:
            return {
                ...state,
                loadingWorkers: true,
                workers: [],
            }
        case `${ACTION_loadWorkersWithMachines}_FULFILLED`:
            return {
                ...state,
                loadingWorkers: false,
                workers: payload.data,
            }
        case `${ACTION_loadWorkersWithMachines}_REJECTED`:
            return {
                ...state,
                loadingWorkers: false,
                workers: [],
            }
        case `${ACTION_loadMachines}_PENDING`:
            return {
                ...state,
                loadingMachines: true,
                machines: [],
            }
        case `${ACTION_loadMachines}_FULFILLED`:
            return {
                ...state,
                loadingMachines: false,
                machines: payload.data,
            }
        case `${ACTION_loadMachines}_REJECTED`:
            return {
                ...state,
                loadingMachines: false,
                machines: [],
            }
        case `${ACTION_loadMos}_PENDING`:
            return {
                ...state,
                loadingMos: true,
                mos: [],
            }
        case `${ACTION_loadMos}_FULFILLED`:
            return {
                ...state,
                loadingMos: false,
                mos: payload.data,
            }
        case `${ACTION_loadMos}_REJECTED`:
            return {
                ...state,
                loadingMos: false,
                mos: [],
            }
        case `${ACTION_sendNewImport}_PENDING`:
            return {
                ...state,
                performingImport: true,
            }
        case `${ACTION_sendNewImport}_FULFILLED`:
            return {
                ...state,
                performingImport: false,
            }
        case `${ACTION_sendNewImport}_REJECTED`:
            return {
                ...state,
                performingImport: false,
            }
        case `${ACTION_resetImport}_PENDING`:
            return {
                ...state,
                performingResetImport: true,
            }
        case `${ACTION_resetImport}_FULFILLED`:
            return {
                ...state,
                performingResetImport: false,
                mos: [],
            }
        case `${ACTION_resetImport}_REJECTED`:
            return {
                ...state,
                performingResetImport: false,
            }
        default:
            return state
    }
}
