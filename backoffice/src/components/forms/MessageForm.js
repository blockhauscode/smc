import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../input'
import Select from 'react-select'

export class MessageForm extends Component {
    constructor(props) {
        super(props)
    }

    onMessageSend = () => {
        this.props.onSubmitForm()
    }

    renderReactDropdown = ({ input }) => {
        const options = this.props.receivers
            .sort((x, y) => (x.firstName < y.firstName ? -1 : 1))
            ?.map((x) => ({
                value: x.id,
                label: `${x.firstName} ${x.lastName}`,
            }))
        return (
            <div className="w-full max-w-screen-sm p-10">
                <Select
                    {...input}
                    isMulti
                    placeholder="Seleziona Destinatario"
                    onChange={(value) => input.onChange(value)}
                    onBlur={() => input.onBlur(input.value)}
                    options={options}
                    isClearable={false}
                    isSearchable={false}
                />
            </div>
        )
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field name="receivers" component={this.renderReactDropdown} />
                <Field
                    placeholder="Oggetto"
                    type="text"
                    name="subject"
                    label="Oggetto"
                    component={Input}
                />
                <Field
                    name="text"
                    component={(props) => (
                        <Input
                            {...props}
                            isArea={true}
                            placeholder="Testo messaggio"
                            type="text"
                            name="text"
                            label="Testo"></Input>
                    )}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onMessageSend}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'MessageForm',
})(MessageForm)
