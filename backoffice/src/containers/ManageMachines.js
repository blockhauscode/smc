import React, { Component } from 'react'

import { bindActionCreators } from 'redux'

import Modal from 'react-modal'
import { withContainer, connect } from '../services/container'
import LoggedNavbar from '../components/loggedNavbar'
import PageTitle from '../components/pageTitle'
import Button from '../components/button'
import Box from '../components/box'
import Alert from '../components/alert'
import Table from '../components/table'
import MachineForm from '../components/forms/MachineForm'
import {
    faEdit,
    faTrash,
    faPlus,
    faTimes,
} from '@fortawesome/free-solid-svg-icons'
import { confirmAlert } from 'react-confirm-alert'
import {
    createMachine,
    editMachine,
    deleteMachine,
    loadAreasWithMachines,
} from '../stores/actions'

export class ManageMachines extends Component {
    state = {
        modalMachine: false, // modal edit and create machine open or close
        machineEditMode: null, // machine that is under edit mode
    }

    componentDidMount() {
        this.props.loadAreasWithMachines()
    }

    openModalMachine(machine) {
        this.setState({
            modalMachine: true,
            machineEditMode: machine,
        })
    }

    closeModalMachine() {
        this.setState({
            modalMachine: false,
            machineEditMode: null,
        })
    }

    onDeleteMachine = (machineId) => this.props.deleteMachine(machineId)

    onMachineFormSubmit = async () => {
        if (this.state.machineEditMode) {
            await this.props.editMachine(this.state.machineEditMode.id)
        } else {
            await this.props.createMachine()
        }
        this.closeModalMachine()
        this.props.loadAreasWithMachines()
    }

    alertConfirmDeleteMachine = (machine) => {
        confirmAlert({
            customUI: ({ onClose }) => (
                <Alert
                    onPositive={async () => {
                        await this.onDeleteMachine(machine.id)
                        onClose()
                        this.props.loadAreasWithMachines()
                    }}
                    onNegative={onClose}
                    title="Sei sicuro di voler eliminare questa macchina?"
                />
            ),
        })
    }

    getInitialValues() {
        if (this.state.machineEditMode) {
            return {
                ...this.state.machineEditMode,
                area: this.props.main.areas
                    ?.map((x) => ({
                        value: x.id,
                        label: x.name,
                    }))
                    .find((x) => x.value === this.state.machineEditMode.areaId),
            }
        }
    }

    render() {
        const { areas } = this.props.main
        return (
            <div>
                <LoggedNavbar
                    links={[{ name: 'Dashboard', to: '/dashboard' }]}
                />
                <PageTitle title="Gestione Macchine" />
                {areas?.map((area, i) =>
                    area.machines.length ? (
                        <Box
                            title={`Macchine in area ${area.name}`}
                            key={`area${i}`}>
                            <Table
                                columns={[
                                    {
                                        title: 'Codice',
                                        contentType: 'itemProp',
                                        key: 'code',
                                    },
                                    {
                                        title: 'Descrizione',
                                        contentType: 'itemProp',
                                        key: 'description',
                                    },
                                    {
                                        title: 'Descrizione breve',
                                        contentType: 'itemProp',
                                        key: 'shortDescription',
                                    },
                                    {
                                        title: 'Φ1',
                                        contentType: 'itemProp',
                                        key: 'f1',
                                    },
                                    {
                                        title: 'Φ2',
                                        contentType: 'itemProp',
                                        key: 'f2',
                                    },
                                    {
                                        title: 'Φ3',
                                        contentType: 'itemProp',
                                        key: 'f3',
                                    },
                                    {
                                        title: 'Operatori',
                                        contentType: 'itemProp',
                                        key: 'workers.length',
                                    },
                                    {
                                        title: 'Azioni',
                                        contentType: 'actions',
                                        actions: [
                                            {
                                                icon: faEdit,
                                                name: 'Modifica',
                                                onClick: (machine) =>
                                                    this.openModalMachine(
                                                        machine,
                                                    ),
                                            },
                                            {
                                                icon: faTrash,
                                                name: 'Elimina',
                                                onClick: this
                                                    .alertConfirmDeleteMachine,
                                            },
                                        ],
                                    },
                                ]}
                                items={area.machines.sort((x, y) =>
                                    x.shortDescription < y.shortDescription
                                        ? -1
                                        : 1,
                                )}></Table>
                        </Box>
                    ) : null,
                )}
                <div className="flex flex-col m-10">
                    <Button
                        onClick={() => this.openModalMachine()}
                        text="Crea Nuova macchina"
                        style="px-4 py-2 self-end"
                    />
                </div>

                {/* Create or edit machine */}
                <Modal
                    isOpen={this.state.modalMachine}
                    onRequestClose={() => this.closeModalMachine()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalMachine()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle
                        title={`${
                            this.state.machineEditMode
                                ? 'Modifica questa macchina'
                                : 'Crea una nuova macchina'
                        }`}
                    />
                    <MachineForm
                        onSubmitForm={this.onMachineFormSubmit}
                        areas={areas}
                        initialValues={this.getInitialValues()}
                        actionText={`${
                            this.state.machineEditMode
                                ? 'Salva Modifiche'
                                : 'Crea Macchina'
                        }`}
                    />
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = ({ main }) => ({ main })

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            createMachine,
            editMachine,
            deleteMachine,
            loadAreasWithMachines,
        },
        dispatch,
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(ManageMachines))
