'use strict'

// 'use strict'

module.exports = function(Worker) {
    Worker.afterRemote('create', (ctx, worker, next) => {
        const { Machine } = Worker.app.models
        // add the worker to the default machines he will work on
        ctx.args.data.machines.forEach(machineId => {
            Machine.findById(machineId).then(machine => {
                machine.__link__workers(worker.id)
            })
        })
        next()
    })

    //patchAttributes
    Worker.afterRemote('replaceById', async (ctx, worker, next) => {
        // unlink all older machine for worker
        const _worker = await Worker.findOne({
            where: { id: worker.id },
            include: 'machines',
        })

        _worker.machines().forEach(machine => {
            _worker.__unlink__machines(machine.id)
        })

        ctx.args.data.machines.forEach(machineId => {
            _worker.__link__machines(machineId)
        })
    })
}
