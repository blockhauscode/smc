import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import Modal from 'react-modal'
import { toast } from 'react-toastify'
import { withContainer, connect } from '../services/container'
import LoggedNavbar from '../components/loggedNavbar'
import PageTitle from '../components/pageTitle'
import Button from '../components/button'
import Box from '../components/box'
import Alert from '../components/alert'
import Table from '../components/table'
import AdminForm from '../components/forms/AdminForm'
import WorkerForm from '../components/forms/WorkerForm'
import {
    faEdit,
    faTrash,
    faPlus,
    faTimes,
} from '@fortawesome/free-solid-svg-icons'
import { confirmAlert } from 'react-confirm-alert'
import {
    deleteWorker,
    editWorker,
    createWorker,
    createAdmin,
    associateMachine,
    loadWorkersWithMachines,
    loadMachines,
} from '../stores/actions'

export class ManageWorkers extends Component {
    state = {
        modalWorker: false, // modal edit and create worker open or close
        workerEditMode: null, // worker that is under edit mode
    }

    componentDidMount() {
        this.props.loadWorkersWithMachines()
        this.props.loadMachines()
    }

    openModalWorker(worker) {
        this.setState({
            modalWorker: true,
            workerEditMode: worker,
        })
    }

    openModalAdmin(worker) {
        this.setState({
            modalAdmin: true,
        })
    }

    closeModalWorker() {
        this.setState({
            modalWorker: false,
            workerEditMode: null,
        })
    }

    closeModalAdmin() {
        this.setState({
            modalAdmin: false,
        })
    }

    onDeleteWorker = (workerId) => this.props.deleteWorker(workerId)

    onWorkerFormSubmit = async () => {
        if (this.state.workerEditMode) {
            await this.props.editWorker(this.state.workerEditMode.id)
        } else {
            await this.props.createWorker()
        }
        // wait 1000 milliseconds for "after remote" linking of machines
        setTimeout(() => {
            this.closeModalWorker()
            this.props.loadWorkersWithMachines()
        }, 1000)
    }

    onAdminFormSubmit = async () => {
        await this.props.createAdmin()
        // wait 1000 milliseconds for "after remote" linking of machines
        setTimeout(() => {
            this.closeModalAdmin()
            toast.success('Admin creato')
        }, 1000)
    }

    alertConfirmDeleteWorker = (worker) => {
        confirmAlert({
            customUI: ({ onClose }) => (
                <Alert
                    onPositive={async () => {
                        await this.onDeleteWorker(worker.id)
                        onClose()
                        this.props.loadWorkersWithMachines()
                    }}
                    onNegative={onClose}
                    title="Sei sicuro di voler eliminare questo operatore?"
                />
            ),
        })
    }

    getInitialValues() {
        if (this.state.workerEditMode) {
            return {
                ...this.state.workerEditMode,
                machines: this.props.main.machines
                    ?.map((x) => ({
                        value: x.id,
                        label: x.shortDescription || x.description,
                    }))
                    .filter((x) =>
                        this.state.workerEditMode.machines.find(
                            (m) => m.id === x.value,
                        ),
                    ),
            }
        }
    }

    render() {
        const { workers } = this.props.main
        return (
            <div>
                <LoggedNavbar
                    links={[{ name: 'Dashboard', to: '/dashboard' }]}
                />
                <PageTitle title="Gestione Operatori" />

                <Box>
                    <Table
                        columns={[
                            {
                                title: 'Folder',
                                contentType: 'itemProp',
                                key: 'folder',
                            },
                            {
                                title: 'Nome',
                                contentType: 'itemProp',
                                key: 'firstName',
                            },
                            {
                                title: 'Cognome',
                                contentType: 'itemProp',
                                key: 'lastName',
                            },
                            {
                                title: 'Email',
                                contentType: 'itemProp',
                                key: 'email',
                            },
                            {
                                title: 'Tagli',
                                contentType: 'itemProp',
                                key: 'tagli',
                            },
                            {
                                title: 'Macchine',
                                contentType: 'method',
                                method: (w) =>
                                    w.machines
                                        ?.map(
                                            (x) =>
                                                x.shortDescription ||
                                                x.description,
                                        )
                                        .join(' '),
                            },
                            {
                                title: 'Azioni',
                                contentType: 'actions',
                                actions: [
                                    {
                                        icon: faEdit,
                                        name: 'Modifica',
                                        onClick: (worker) =>
                                            this.openModalWorker(worker),
                                    },
                                    {
                                        icon: faTrash,
                                        name: 'Elimina',
                                        onClick: this.alertConfirmDeleteWorker,
                                    },
                                ],
                            },
                        ]}
                        items={workers.sort((x, y) =>
                            x.firstName < y.firstName ? -1 : 1,
                        )}></Table>
                </Box>

                <div className="flex flex-col m-10">
                    <Button
                        onClick={() => this.openModalWorker()}
                        text="Crea Operatore"
                        style="px-4 py-2 self-end"
                    />
                </div>

                <div className="flex flex-col m-10">
                    <Button
                        onClick={() => this.openModalAdmin()}
                        text="Crea Admin"
                        style="px-4 py-2 self-end"
                    />
                </div>

                {/* Create or edit worker */}
                <Modal
                    isOpen={this.state.modalWorker}
                    onRequestClose={() => this.closeModalWorker()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalWorker()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle
                        title={`${
                            this.state.workerEditMode
                                ? 'Modifica operatore'
                                : 'Inserisci nuovo operatore'
                        }`}
                    />
                    <WorkerForm
                        onSubmitForm={this.onWorkerFormSubmit}
                        machines={this.props.main.machines}
                        initialValues={this.getInitialValues()}
                        actionText={`${
                            this.state.workerEditMode
                                ? 'Salva Modifiche'
                                : 'Crea Operatore'
                        }`}
                    />
                </Modal>

                <Modal
                    isOpen={this.state.modalAdmin}
                    onRequestClose={() => this.closeModalAdmin()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() =>
                                this.closeModalWcloseModalAdminorker()
                            }
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle title={'Inserisci nuovo admin'} />
                    <AdminForm
                        onSubmitForm={this.onAdminFormSubmit}
                        actionText={'Crea Admin'}
                    />
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = ({ main }) => ({ main })

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            deleteWorker,
            editWorker,
            createWorker,
            createAdmin,
            associateMachine,
            loadWorkersWithMachines,
            loadMachines,
        },
        dispatch,
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(ManageWorkers))
