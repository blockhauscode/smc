import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import Modal from 'react-modal'
import moment from 'moment'
import { withContainer, connect } from '../services/container'
import DataTable from 'react-data-table-component'
import LoggedNavbar from '../components/loggedNavbar'
import PageTitle from '../components/pageTitle'
import Button from '../components/button'
import { faTrash, faTimes } from '@fortawesome/free-solid-svg-icons'
import MessageForm from '../components/forms/MessageForm'
import { confirmAlert } from 'react-confirm-alert'
import {
    loadWorkersWithMachines,
    sendMessages,
    fetchMessages,
    editMessage,
} from '../stores/actions'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'

export class MessageCenter extends Component {
    state = {
        modalMessage: false,
        modalText: false,
    }

    componentDidMount() {
        this.props.loadWorkersWithMachines()
        setInterval(() => {
            this.props.fetchMessages()
        }, 1000)
    }

    closeModalMessage() {
        this.setState({
            modalMessage: false,
        })
    }
    openModalMessage(worer) {
        this.setState({
            modalMessage: true,
        })
    }

    openModalText(text) {
        this.setState({
            modalText: text,
        })
    }

    closeModalText() {
        this.setState({
            modalText: false,
        })
    }

    onMessageFormSubmit = async () => {
        await this.props.sendMessages()
        // wait 1000 milliseconds for "after remote" linking of machines
        setTimeout(() => {
            this.closeModalMessage()

            // reload all messages...?
            // this.props.loadWorkersWithMachines()
        }, 1000)
    }

    getColumnsSent() {
        return [
            {
                name: 'Data',
                selector: 'createdAt',
                sortable: true,
                format: (row, index) =>
                    moment(row.createdAt).format('DD/MM/YYYY HH:mm'),
            },
            {
                name: 'Destinatario',
                selector: 'receiver.email',
                sortable: true,
            },
            {
                name: 'Oggetto',
                selector: 'subject',
                sortable: true,
            },
            {
                name: 'Messaggio',
                cell: (row) => (
                    <div
                        style={{ cursor: 'pointer' }}
                        onClick={() => this.openModalText(row.text)}>
                        {row.text.replace(/(.{20})..+/, '$1...')}
                    </div>
                ),
            },
            {
                sortable: false,
                cell: (row) => (
                    <Button
                        onClick={() => {
                            this.props.editMessage(row.id)
                        }}
                        icon={faTrash}
                        text="Remove"
                        style="px-4 py-2 self-end"></Button>
                ),
            },
        ]
    }

    getColumnsReceived() {
        return [
            {
                name: 'Data',
                selector: 'createdAt',
                sortable: true,
                format: (row, index) =>
                    moment(row.createdAt).format('DD/MM/YYYY HH:mm'),
            },
            {
                name: 'Mittente',
                selector: 'sender.email',
                sortable: false,
            },
            {
                name: 'Oggetto',
                selector: 'subject',
                sortable: false,
            },
            {
                name: 'Messaggio',
                cell: (row) => (
                    <div
                        style={{ cursor: 'pointer' }}
                        onClick={() => this.openModalText(row.text)}>
                        {row.text.replace(/(.{20})..+/, '$1...')}
                    </div>
                ),
            },
            {
                sortable: false,
                cell: (row) => (
                    <Button
                        onClick={() => {
                            this.props.editMessage(row.id)
                        }}
                        icon={faTrash}
                        text="Remove"
                        style="px-4 py-2 self-end"></Button>
                ),
            },
        ]
    }

    render() {
        const { workers } = this.props.main
        return (
            <div>
                <LoggedNavbar
                    links={[{ name: 'Dashboard', to: '/dashboard' }]}
                />
                <PageTitle title="Centro messagggi" />

                <div className="flex flex-col m-10">
                    <Button
                        onClick={() => this.openModalMessage()}
                        text="Nuovo Messaggio"
                        style="px-4 py-2 self-end"
                    />
                    <Tabs>
                        <TabList>
                            <Tab>Ricevuti</Tab>
                            <Tab>Inviati</Tab>
                        </TabList>

                        <TabPanel>
                            <DataTable
                                columns={this.getColumnsReceived()}
                                data={this.props.messages.received.filter(
                                    (x) => !x.hidden,
                                )}
                                pagination
                                paginationPerPage={50}
                                paginationRowsPerPageOptions={[50, 100, 300]}
                                paginationComponentOptions={{
                                    rowsPerPageText: 'Righe per pagina:',
                                    rangeSeparatorText: 'di',
                                    noRowsPerPage: false,
                                }}
                            />
                        </TabPanel>
                        <TabPanel>
                            <DataTable
                                columns={this.getColumnsSent()}
                                data={this.props.messages.sent.filter(
                                    (x) => !x.hidden,
                                )}
                                pagination
                                paginationPerPage={50}
                                paginationRowsPerPageOptions={[50, 100, 300]}
                                paginationComponentOptions={{
                                    rowsPerPageText: 'Righe per pagina:',
                                    rangeSeparatorText: 'di',
                                    noRowsPerPage: false,
                                }}
                            />
                        </TabPanel>
                    </Tabs>
                </div>

                {/* Send a new message */}
                <Modal
                    isOpen={this.state.modalMessage}
                    onRequestClose={() => this.closeModalMessage()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalMessage()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle title={`Invia un messaggio`} />
                    <MessageForm
                        onSubmitForm={this.onMessageFormSubmit}
                        receivers={workers}
                        actionText={`Invia`}
                    />
                </Modal>

                <Modal
                    isOpen={this.state.modalText}
                    onRequestClose={() => this.closeModalText()}
                    className="modal"
                    ariaHideApp={false}
                    overlayClassName="overlay">
                    <div className="flex flex-col m-4">
                        <Button
                            onClick={() => this.closeModalText()}
                            text="Chiudi"
                            icon={faTimes}
                            style="px-4 py-2 self-end"
                        />
                    </div>
                    <PageTitle title={'Nota'} />
                    <div className="flex flex-row p-4 text-center items-center justify-center">
                        {this.state.modalText}
                    </div>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = ({ main, messages }) => ({ main, messages })

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            loadWorkersWithMachines,
            sendMessages,
            fetchMessages,
            editMessage,
        },
        dispatch,
    )

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(withContainer(MessageCenter))
