import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../input'

export class PathForm extends Component {
    constructor(props) {
        super(props)
    }

    onPathFormButtonPress = () => {
        this.props.onSubmitForm()
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    placeholder=""
                    type="text"
                    name="path"
                    label="path"
                    component={Input}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onPathFormButtonPress}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'PathForm',
})(PathForm)
