const app = require('../../server/server')

module.exports = function (Account) {
    Account.prototype.setRole = async function (name) {
        /* Unrelated models */
        const { Role, RoleMapping } = app.models

        /* Reset account roles */
        await this.map.destroyAll({})

        /* Create role relation */
        const roleInstance = await Role.findOne({
            where: { name: name || this.role },
        })

        roleInstance.principals.create({
            principalType: RoleMapping.USER,
            principalId: this.id,
        })
    }

    /**
     * Hook triggered after each instance save operation
     */
    Account.observe('after save', (ctx, next) => {
        /* Set default role for new instances of account */
        if (ctx.isNewInstance) ctx.instance.setRole(ctx.instance.role)
        next()
    })
}
