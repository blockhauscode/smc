import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../../components/input'

const validate = values => {
    const errors = {}
    if (!values.email) {
        errors.email = 'Required'
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }
    if (!values.password) {
        errors.password = 'Required'
    } else if (values.password.length > 20) {
        errors.password = 'Must be 20 characters or less'
    }
    return errors
}

export class Login extends Component {
    constructor(props) {
        super(props)
    }

    onLoginSubmit = () => {
        this.props.onSubmitForm()
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    placeholder="Email"
                    type="email"
                    name="email"
                    label="Email"
                    component={Input}
                />
                <Field
                    placeholder="Password"
                    type="password"
                    name="password"
                    label="Password"
                    autoComplete="off"
                    component={Input}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onLoginSubmit}>
                    Sign in
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'Login',
    validate,
})(Login)
