module.exports = () => {
  return function refreshToken(req, _res, next) {
    /* Skip non authenticated calls */
    const { accessToken } = req
    if (!accessToken) return next()

    /* Refresh token on authenticated calls */
    accessToken.created = new Date()
    return accessToken.save(next)
  }
}
