import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../input'
import Select from 'react-select'

const validate = (values) => {
    const errors = {}
    if (!values.name) {
        errors.name = 'Required'
    } else if (values.name.length > 20) {
        errors.name = 'Must be 20 characters or less'
    }
    return errors
}

export class WorkerForm extends Component {
    constructor(props) {
        super(props)
    }

    onWorkerFormButtonPress = () => {
        this.props.onSubmitForm()
    }

    renderReactDropdown = ({ input }) => {
        const options = this.props.machines?.map((x) => ({
            value: x.id,
            label: x.shortDescription || x.description,
        }))
        return (
            <div className="w-full max-w-screen-sm">
                <Select
                    {...input}
                    isMulti
                    placeholder="Seleziona Macchine"
                    onChange={(value) => input.onChange(value)}
                    onBlur={() => input.onBlur(input.value)}
                    options={options}
                    isClearable={false}
                    isSearchable={false}
                />
            </div>
        )
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    placeholder="01"
                    type="text"
                    name="folder"
                    label="Numero cartella"
                    component={Input}
                />
                <Field
                    placeholder="Mario"
                    type="text"
                    name="firstName"
                    label="Nome"
                    component={Input}
                />
                <Field
                    placeholder="Rossi"
                    type="text"
                    name="lastName"
                    label="Cognome"
                    component={Input}
                />
                <Field
                    type="text"
                    name="tagli"
                    label="Tagli"
                    component={Input}
                />
                <Field
                    placeholder="mario.rossi@smc.com"
                    type="text"
                    name="email"
                    label="Email"
                    component={Input}
                />
                <Field name="machines" component={this.renderReactDropdown} />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onWorkerFormButtonPress}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'WorkerForm',
    validate,
})(WorkerForm)
