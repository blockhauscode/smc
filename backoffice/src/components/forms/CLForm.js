import React, { Component } from 'react'
import { reduxForm, Field } from 'redux-form'
import Input from '../input'

export class CLForm extends Component {
    constructor(props) {
        super(props)
    }

    onNoteFormButtonPress = () => {
        this.props.onSubmitForm()
    }

    render() {
        return (
            <div className="flex flex-col items-center">
                <Field
                    placeholder=""
                    type="number"
                    name="cl"
                    label="Anticipo"
                    component={Input}
                />
                <button
                    className="btn btn-blue"
                    type="button"
                    onClick={this.onNoteFormButtonPress}>
                    {this.props.actionText}
                </button>
            </div>
        )
    }
}

export default reduxForm({
    form: 'CLForm',
})(CLForm)
