import React from 'react'

const Box = ({ title, style, children, actions }) => {
    const defaultStyle = 'flex flex-col p-4 '
    return (
        <div className="m-10 bg-indigo-100 border-gray-600 border ">
            <div className="text-blue px-8 py-2 flex flex-row justify-center items-center">
                <h1>{title}</h1> {actions}
            </div>
            <div className={defaultStyle + style}>{children}</div>
        </div>
    )
}

export default Box
