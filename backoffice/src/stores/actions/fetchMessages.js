import { ACTION_fetchMessages } from './types'
import 'moment/locale/it'

const fetchMessages = ({ rest }) => (dispatch, getState) => {
    const config = {
        params: {
            access_token: getState().user.token,
            filter: {
                order: 'createdAt DESC',
                include: ['sender', 'receiver'],
                where: {
                    or: [
                        {
                            receiverId: getState().user.id,
                        },
                        {
                            senderId: getState().user.id,
                        },
                    ],
                },
            },
        },
    }
    if (getState().messages.lastUpdate) {
        config.params.filter.where.createdAt = {
            gt: getState().messages.lastUpdate,
        }
    }
    console.log(config)
    const request = rest.get(`/Messages`, config)

    return dispatch({
        payload: request,
        type: ACTION_fetchMessages,
    })
}

export default fetchMessages
